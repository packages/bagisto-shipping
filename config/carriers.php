<?php

return [
    'coupe'    => [
        'code'        => 'coupe',
        'title'       => 'Coupe',
        'description' => 'Coupe Shipping',
        'active'      => true,
        'class'       => 'DFM\Shipping\Carriers\Coupe',
    ],

    'leleu'    => [
        'code'        => 'leleu',
        'title'       => 'Leleu',
        'description' => 'Leleu Shipping',
        'active'      => true,
        'class'       => 'DFM\Shipping\Carriers\Leleu',
    ],

    'la-poste' => [
        'code'        => 'la-poste',
        'title'       => 'La Poste',
        'description' => 'La Poste Shipping',
        'active'      => true,
        'class'       => 'DFM\Shipping\Carriers\LaPoste',
    ],
];
