<?php

return [
    'pallet' => [
        'length' => 800,
        'width'  => 1200,
        'height' => 2000,
    ],

    'la-poste' => [
        'max' => env('LA_POSTE_SHIPPING_MAX', 30),
        'min' => env('LA_POSTE_SHIPPING_MIN', 0),
    ],

    'coupe' => [
        'max' => env('COUPE_SHIPPING_MAX', 99999),
        'min' => env('COUPE_SHIPPING_MIN', 0),
    ]
];
