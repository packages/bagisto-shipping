<?php

return [
    [
        'key'        => 'settings.carriers',
        'name'       => 'dfm-shipping::app.layout.carriers',
        'route'      => 'admin.carriers.la-poste.index',
        'sort'       => 9,
        'icon-class' => '',
    ],
    [
        'key'        => 'settings.carriers.la-poste',
        'name'       => 'dfm-shipping::app.layout.carrier-la-poste',
        'route'      => 'admin.carriers.la-poste.index',
        'sort'       => 1,
        'icon-class' => '',
    ],
    [
        'key'        => 'settings.carriers.coupe',
        'name'       => 'dfm-shipping::app.layout.carrier-coupe',
        'route'      => 'admin.carriers.coupe.index',
        'sort'       => 2,
        'icon-class' => '',
    ],
    [
        'key'        => 'settings.carriers.leleu',
        'name'       => 'dfm-shipping::app.layout.carrier-leleu',
        'route'      => 'admin.carriers.leleu.index',
        'sort'       => 3,
        'icon-class' => '',
    ],
];
