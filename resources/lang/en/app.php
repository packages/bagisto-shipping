<?php

return [
    'admin'     => [
        'system'    => [
            'coupe-shipping'    => 'Coupe Shipping',
            'leleu-shipping'    => 'Leleu Shipping',
            'la-poste-shipping' => 'La Poste Shipping',
        ]
    ],

    'layout'    => [
        'carriers'          => 'Carriers',
        'carrier-la-poste'  => 'The Post Office',
        'carrier-coupe'     => 'Coupe',
        'carrier-leleu'     => 'Leleu',
    ],
];
