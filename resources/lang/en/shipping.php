<?php

return [
    'title'         => 'Carrier',

    'fields'    => [
        'weight'    => 'Weight',
        'pallet'    => 'Pallet',
        'price'     => 'Price',
        'state'     => 'State',
    ],
];
