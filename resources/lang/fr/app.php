<?php

return [
    'admin'     => [
        'system'    => [
            'coupe-shipping'    => 'Expédition Coupe',
            'leleu-shipping'    => 'Expédition Leleu',
            'la-poste-shipping' => 'Expédition La Poste',
        ]
    ],

    'layout'    => [
        'carriers'          => 'Transporteurs',
        'carrier-la-poste'  => 'La Poste',
        'carrier-coupe'     => 'Coupe',
        'carrier-leleu'     => 'Leleu',
    ],
];
