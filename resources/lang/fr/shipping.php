<?php

return [
    'title'         => 'Transporteur',

    'fields'    => [
        'weight'    => 'Poids',
        'pallet'    => 'Palette',
        'price'     => 'Prix',
        'state'     => 'Etat',
    ],
];
