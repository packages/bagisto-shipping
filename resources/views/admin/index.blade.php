@extends('admin::layouts.content')

@section('page_title')
    @lang('dfm-shipping::shipping.title')
@stop

@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>@lang('dfm-shipping::shipping.title')</h1>
            </div>
        </div>

        <div class="page-content">
            {!! $dataGrid->render() !!}
        </div>
    </div>
@stop
