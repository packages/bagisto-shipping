<?php

Route::middleware('admin', 'bindings')
    ->namespace('DFM\Shipping\Http\Controllers\Admin')
    ->prefix('admin')
    ->name('admin.')
    ->group(function () {
        Route::prefix('carriers')->name('carriers.')->group(function () {
            Route::resource('la-poste', 'LaPosteController')->only(['index']);
            Route::resource('coupe', 'CoupeController')->only(['index']);
            Route::resource('leleu', 'LeleuController')->only(['index']);
        });
    });
