<?php

namespace DFM\Shipping\Carriers;

use Webkul\Attribute\Repositories\AttributeOptionRepository;
use Webkul\Checkout\Models\Cart;
use Webkul\Shipping\Carriers\AbstractShipping as BaseAbstractShipping;

abstract class AbstractShipping extends BaseAbstractShipping
{
    /**
     * @param  Cart  $cart
     * @return string
     */
    public function getCartCarrier(Cart $cart)
    {
        $data = [];

        foreach ($cart->items as $item) {
            if ($carrier = $item->product->carrier) {
                $data[] = app(AttributeOptionRepository::class)->find($carrier)->swatch_value;
            }
        }

        foreach (['leleu', 'coupe', 'la-poste'] as $value) {
            if (in_array($value, $data)) {
                return $value;
            }
        }

        return '';
    }

    /**
     * @param  Cart  $cart
     * @return bool
     */
    public function isAvailable2(Cart $cart)
    {
        $code = $this->getCartCarrier($cart);

        return in_array($code, ['', $this->code]);
    }
}
