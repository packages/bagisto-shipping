<?php

namespace DFM\Shipping\Carriers;

use DFM\Shipping\Models\LeleuPrice;
use Webkul\Checkout\Facades\Cart;
use Webkul\Checkout\Models\Cart as CartModel;
use Webkul\Checkout\Models\CartShippingRate;
use Webkul\Core\Models\Address;
use Webkul\Core\Models\CountryState;
use Webkul\Product\Models\Product;

/**
 * Class Leleu
 *
 * @package DFM\Shipping\Carriers
 */
class Leleu extends AbstractShipping
{
    /**
     * Shipment method code
     *
     * @var string
     */
    protected $code = 'leleu';

    /**
     * @return false|CartShippingRate
     */
    public function calculate()
    {
        if (!$this->isAvailable()) {
            return false;
        }

        $cart = Cart::getCart();

        if (!in_array($this->getCartCarrier($cart), ['', 'la-poste', 'coupe', $this->code])) {
            return false;
        }

        if (blank($cartShippingContainers = $this->getCartShippingContainers($cart))) {
            return false;
        }

        if (!($price = $this->getCartShippingPrice($cart->shipping_address, $cartShippingContainers))) {
            return false;
        }

        $object = new CartShippingRate();

        $object->carrier = 'leleu';
        $object->carrier_title = $this->getConfigData('title');
        $object->method = 'leleu_leleu';
        $object->method_title = $this->getConfigData('title');
        $object->method_description = $this->getConfigData('description');
        $object->price = $price;
        $object->base_price = $price;

        return $object;
    }

    /**
     * @param  CartModel  $cart
     * @return array
     */
    private function getCartShippingContainers(CartModel $cart)
    {
        $data = [];
        $items = $cart->items->sortByDesc('product.palette_percent');

        foreach ($items as $item) {
            if (!($product = $item->product) || !$product->palette_number || !$product->palette_percent) {
                return $data;
            }

            $this->pushToCartShippingContainers($data, $product, $item->quantity);
        }

        return $data;
    }

    /**
     * @param  array    $data
     * @param  Product  $product
     * @param  int      $itemQty
     */
    private function pushToCartShippingContainers(array &$data, Product $product, int $itemQty)
    {
        if ($product->palette_number >= $itemQty) {
            if (($product->palette_number == $itemQty && $product->palette_percent <= 100) ||
                ($product->palette_number > $itemQty && $product->palette_percent == 100)
            ) {
                for ($i = 0; ; $i++) {
                    for ($j = 0; $j < 6; $j++) {
                        if (!isset($data[$i][$j])) {
                            $data[$i][$j][] = 100;
                            break 2;
                        }
                    }
                }
            } elseif ($product->palette_number > $itemQty && $product->palette_percent < 100) {
                for ($k = 0; $k < $itemQty; $k++) {
                    for ($i = 0; ; $i++) {
                        for ($j = 0; $j < 6; $j++) {
                            if (!isset($data[$i][$j]) || (100 - array_sum($data[$i][$j])) >= $product->palette_percent) {
                                $data[$i][$j][] = $product->palette_percent;
                                break 2;
                            }
                        }
                    }
                }
            } elseif ($product->palette_percent > 100) {
                $pallets = ceil($product->palette_percent / 100);
                for ($i = 0; ; $i++) {
                    if (!isset($data[$i]) || (6 - count($data[$i])) >= $pallets) {
                        for ($j = 0; $j < $pallets; $j++) {
                            $data[$i][][] = 100;
                        }
                        break;
                    }
                }
            }
        } else {
            $this->pushToCartShippingContainers($data, $product, $product->palette_number);
            $this->pushToCartShippingContainers($data, $product, $itemQty - $product->palette_number);
        }
    }

    /**
     * @param  Address  $shippingAddress
     * @param  array    $containers
     * @return int
     */
    private function getCartShippingPrice(Address $shippingAddress, array $containers)
    {
        $price = 0;
        $state = CountryState::where([
            ['country_code', '=', $shippingAddress->country],
            ['code', '=', $shippingAddress->state],
        ])->first();

        foreach ($containers as $container) {
            $leleuPrice = LeleuPrice::where([
                ['volume', '>=', count($container)],
                ['state_id', '=', @($state->id)],
            ])->orderBy('price')->first();

            $price += @($leleuPrice->price);
        }

        return $price;
    }
}
