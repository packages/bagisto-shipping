<?php

namespace DFM\Shipping\Console\Commands;

use DFM\Shipping\Imports\CoupePricesImport;
use DFM\Shipping\Imports\LaPostePricesImport;
use DFM\Shipping\Imports\LeleuPricesImport;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class ImportPrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dfm-shipping:import {carrier} {--path=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import coupe|leleu|la-poste prices via a excel file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (! in_array($carrier = $this->argument('carrier'), $carriers = ['coupe', 'leleu', 'la-poste'])) {
            $this->output->error('The carrier must be a ' . implode(' or ', $carriers) . '.');
            return ;
        }

        switch ($carrier) {
            case 'coupe':
                $defaultPath = '/imports/coupe.xlsx';
                $importClass = new CoupePricesImport();
                break;
            case 'leleu':
                $defaultPath = '/imports/leleu.xls';
                $importClass = new LeleuPricesImport();
                break;
            case 'la-poste':
                $defaultPath = '/imports/la-poste.csv';
                $importClass = new LaPostePricesImport();
                break;
            default:
                $defaultPath = '';
                $importClass = null;
        }

        if (! ($filePath = $this->option('path'))) {
            $filePath = $this->ask('Path', $defaultPath);
        }

        if (! File::isFile($filePath = public_path($filePath)) ||
            ! in_array(File::extension($filePath), ['csv', 'xls', 'xlsx'])) {
            $this->output->error("Path '{$filePath}' doesn't exists or is invalid!");
            return ;
        }

        $this->output->title('Starting import');
        $importClass->import($filePath);
        $this->output->success('Import successful');
    }
}
