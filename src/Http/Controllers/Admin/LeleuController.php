<?php

namespace DFM\Shipping\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DFM\Shipping\DataGrids\LeleuDataGrid;

class LeleuController extends Controller
{
    /**
     * Display a listing of the leleu.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $dataGrid = app(LeleuDataGrid::class);

        return view('dfm-shipping::admin.index', compact('dataGrid'));
    }
}
