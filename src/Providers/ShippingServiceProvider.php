<?php

namespace DFM\Shipping\Providers;

use DFM\Shipping\Console\Commands\ImportPrices;
use Illuminate\Support\ServiceProvider;

class ShippingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../../config/carriers.php', 'carriers'
        );

        $this->mergeConfigFrom(
            __DIR__.'/../../config/system.php', 'core'
        );

        $this->mergeConfigFrom(
            __DIR__.'/../../config/menu-admin.php', 'menu.admin'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'dfm-shipping');
        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'dfm-shipping');

        if ($this->app->runningInConsole()) {
            $this->commands([
                ImportPrices::class,
            ]);
        }

        $this->publishes([
            __DIR__.'/../../config/dfm-shipping.php' => config_path('dfm-shipping.php')
        ], 'shipping-config');

        $this->publishes([
            __DIR__.'/../../resources/imports' => public_path('imports'),
        ], 'shipping-data');
    }
}
